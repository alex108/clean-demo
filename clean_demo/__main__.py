from flask import Flask

from clean_demo.database.sqlalchemy import SqlAlchemyDatabase
from clean_demo.auth.web import bootstrap_auth
from clean_demo.board.web import bootstrap_board

database = SqlAlchemyDatabase("sqlite:///test.db")

board_controller = bootstrap_board(database)
auth_controller = bootstrap_auth(database)

app = Flask(__name__)
app.secret_key = "it's a secret"
app.register_blueprint(board_controller.blueprint)
app.register_blueprint(auth_controller.blueprint)

if __name__ == "__main__":
    app.run(debug=True)
