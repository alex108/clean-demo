class DuplicateUsernameError(Exception):
    """ The username being registered is already taken. """


class InvalidLoginError(Exception):
    """ The given credentials do not match an existing user. """
