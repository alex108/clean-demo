from typing import NamedTuple

__all__ = ["RegisterRequest", "LoginRequest"]


class RegisterRequest(NamedTuple):
    username: str
    password: str
    email: str


class LoginRequest(NamedTuple):
    username: str
    password: str
