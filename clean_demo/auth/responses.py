from typing import NamedTuple

from clean_demo.model import User

__all__ = ["RegisterResponse", "LoginResponse"]


class RegisterResponse(NamedTuple):
    user: User


class LoginResponse(NamedTuple):
    user: User
