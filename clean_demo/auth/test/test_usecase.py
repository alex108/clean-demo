import unittest.mock as mock
import pytest

from clean_demo.auth import AuthUseCase
from clean_demo.auth.requests import RegisterRequest, LoginRequest
from clean_demo.database import Database, DatabaseSession
from clean_demo.auth.errors import DuplicateUsernameError, InvalidLoginError
from clean_demo.model import User


@pytest.fixture(scope="module")
def database():
    database_mock = mock.MagicMock(Database)
    yield database_mock


@pytest.fixture(scope="module")
def sess():
    sess_mock = mock.MagicMock(DatabaseSession)
    yield sess_mock


def test_user_is_registered(database: Database, sess: DatabaseSession):
    sess.query(User).filter_by(username="username").one_or_none.return_value = None
    component = AuthUseCase(database)
    resp = component.register(RegisterRequest(
        username="username",
        password="password",
        email="email@email.com"
    ), sess=sess)
    assert sess.add.called
    assert sess.commit.called
    assert resp.user.username == "username"
    assert resp.user.email == "email@email.com"


def test_duplicate_username_fails(database: Database, sess: DatabaseSession):
    sess.query(User).filter_by(username="username").one_or_none.return_value = User(
        user_id=1,
        username="username",
        password="password",
        email="email@email.com"
    )
    component = AuthUseCase(database)
    with pytest.raises(DuplicateUsernameError):
        component.register(RegisterRequest(
            username="username",
            password="password",
            email="email@email.com"
        ), sess)


def test_correct_login(database: Database, sess: DatabaseSession):
    sess.query(User).filter_by(username="username").one_or_none.return_value = User(
        user_id=1,
        username="username",
        password="password",
        email="email@email.com"
    )
    component = AuthUseCase(database)
    resp = component.login(LoginRequest(username="username", password="password"), sess)
    assert resp.user.user_id == 1
    assert resp.user.username == "username"
    assert resp.user.email == "email@email.com"


def test_bad_username_login(database: Database, sess: DatabaseSession):
    sess.query(User).filter_by(username="username").one_or_none.return_value = None
    component = AuthUseCase(database)
    with pytest.raises(InvalidLoginError):
        component.login(LoginRequest(username="username", password="password"), sess)


def test_bad_password_login(database: Database, sess: DatabaseSession):
    sess.query(User).filter_by(username="username").one_or_none.return_value = User(
        user_id=1,
        username="username",
        password="password",
        email="email@email.com"
    )
    component = AuthUseCase(database)
    with pytest.raises(InvalidLoginError):
        component.login(LoginRequest(username="username", password="bad"), sess)
