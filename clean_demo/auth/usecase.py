from clean_demo.model.user import User
from clean_demo.database import Database, DatabaseSession
from clean_demo.database.common import usesession, unitofwork

from .errors import DuplicateUsernameError, InvalidLoginError
from .requests import *
from .responses import *


class AuthUseCase(object):
    def __init__(self, database: Database) -> None:
        self.database = database

    @usesession
    def register(self, request: RegisterRequest, sess: DatabaseSession = None) -> RegisterResponse:
        dupe = sess.query(User).filter_by(username=request.username).one_or_none()
        if dupe:
            raise DuplicateUsernameError
        user = User(username=request.username, password=request.password, email=request.email)
        with unitofwork(sess, user):
            sess.add(user)
        return RegisterResponse(
            user=user,
        )

    @usesession
    def login(self, request: LoginRequest, sess: DatabaseSession = None) -> LoginResponse:
        user = sess.query(User).filter_by(username=request.username).one_or_none()
        if user is None:
            raise InvalidLoginError
        if not user.check_password(request.password):
            raise InvalidLoginError
        return LoginResponse(
            user=user,
        )
