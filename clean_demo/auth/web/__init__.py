from clean_demo.auth import AuthUseCase
from clean_demo.database import Database

from .controller import AuthController


def bootstrap_auth(database: Database) -> AuthController:
    component = AuthUseCase(database)
    return AuthController(component)
