from flask import g, session, redirect, render_template
from flask.blueprints import Blueprint

from clean_demo.web.common import Router, route, validate

from clean_demo.auth import AuthUseCase
from clean_demo.auth.requests import *

from .validator import register_validator, login_validator


class AuthController(Router):
    blueprint = Blueprint("auth", __name__, url_prefix="/auth", template_folder="templates")

    def __init__(self, component: AuthUseCase) -> None:
        super(AuthController, self).__init__(self.blueprint)
        self.component = component

    @route("/", methods=["GET"])
    def login_page(self):
        return render_template("login.html")

    @route("/register", methods=["POST"])
    @validate(register_validator)
    def register(self):
        req = RegisterRequest(
            username=g.body["username"],
            password=g.body["password"],
            email=g.body["password"],
        )
        resp = self.component.register(req)
        session["user_id"] = resp.user.user_id
        session["username"] = resp.user.username
        return redirect("/")

    @route("/login", methods=["POST"])
    @validate(login_validator)
    def login(self):
        req = LoginRequest(
            username=g.body["username"],
            password=g.body["password"],
        )
        resp = self.component.login(req)
        session["user_id"] = resp.user.user_id
        session["username"] = resp.user.username
        return redirect("/")

    @route("/logout", methods=["GET", "POST"])
    def logout(self):
        session.clear()
        return redirect("/")
