from cerberus import Validator

__all__ = ["register_validator", "login_validator"]


register_schema = {
    "username": {"type": "string", "empty": False},
    "password": {"type": "string", "empty": False},
    "email": {"type": "string", "regex": "(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", "empty": False},
}
register_validator = Validator(register_schema)

login_schema = {
    "username": {"type": "string", "empty": False},
    "password": {"type": "string", "empty": False},
}
login_validator = Validator(login_schema)
