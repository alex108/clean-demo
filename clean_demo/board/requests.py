from typing import NamedTuple

__all__ = ["DoPostRequest"]


class DoPostRequest(NamedTuple):
    user_id: int
    content: str
