from typing import NamedTuple, Collection

from clean_demo.model import Post

__all__ = ["RenderBoardResponse"]


class RenderBoardResponse(NamedTuple):
    posts: Collection[Post]
