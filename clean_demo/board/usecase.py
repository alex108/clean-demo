from clean_demo.database import Database, DatabaseSession
from clean_demo.database.common import usesession, unitofwork
from clean_demo.model import Post, User

from .requests import *
from .responses import *


class BoardUseCase(object):
    def __init__(self, database: Database) -> None:
        self.database = database

    @usesession
    def render_board(self, sess: DatabaseSession) -> RenderBoardResponse:
        posts = sess.query(Post).order_by(
            self.database.order.ascending(Post.post_id)
        ).options(
            self.database.loader.load_one_to_one(Post.author)
        ).all()
        return RenderBoardResponse(
            posts=posts
        )

    @usesession
    def do_post(self, request: DoPostRequest, sess: DatabaseSession) -> None:
        author = None
        if request.user_id:
            author = sess.query(User).filter_by(user_id=request.user_id).one()
        post = Post(content=request.content, author=author)
        with unitofwork(sess):
            sess.add(post)
        return
