from clean_demo.board import BoardUseCase
from clean_demo.database import Database

from .controller import BoardController


def bootstrap_board(database: Database) -> BoardController:
    component = BoardUseCase(database)
    return BoardController(component)
