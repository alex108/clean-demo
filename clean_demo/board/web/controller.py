from flask import g, session, render_template, redirect
from flask.blueprints import Blueprint

from clean_demo.board import BoardUseCase
from clean_demo.board.requests import DoPostRequest
from clean_demo.web.common import Router, route, validate

from .validator import do_post_validator


class BoardController(Router):
    blueprint = Blueprint("board", __name__, template_folder="templates")

    def __init__(self, component: BoardUseCase) -> None:
        super().__init__(self.blueprint)
        self.component = component

    @route("/", methods=["GET"])
    def render_board(self):
        resp = self.component.render_board()
        username = None
        if "user_id" in session:
            username = session["username"]
        return render_template("index.html", posts=resp.posts, username=username)

    @route("/post", methods=["POST"])
    @validate(do_post_validator)
    def do_post(self):
        user_id = None
        if "user_id" in session:
            user_id = session["user_id"]
        self.component.do_post(DoPostRequest(
            user_id=user_id,
            content=g.body["content"]
        ))
        return redirect("/")
