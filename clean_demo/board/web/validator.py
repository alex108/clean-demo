from cerberus import Validator

__all__ = ["do_post_validator"]


do_post_schema = {
    "content": {"type": "string", "empty": False},
}
do_post_validator = Validator(do_post_schema)
