from contextlib import contextmanager
from functools import wraps
from inspect import signature

from typing_extensions import Protocol

from . import Database, DatabaseSession


class UsesDatabase(Protocol):
    database: Database


def usesession(f):
    """ Inject a database session into the given method. The class must have
        a Database wrapper object in its instance.
        The decorated function must either accept **kwargs or a 'sess' parameter. """
    @wraps(f)
    def wrapper(self: UsesDatabase, *args, **kwargs):
        sess = None
        # Allow injecting a session into the method for testing
        if "sess" not in signature(f).bind_partial(self, *args, **kwargs).arguments:
            sess = self.database.get_session()
            kwargs["sess"] = sess
        result = f(self, *args, **kwargs)
        if sess:
            # Clean up the session if set here, else leave it to the caller
            self.database.remove_session()
        return result

    return wrapper


@contextmanager
def unitofwork(session: DatabaseSession, *args):
    """ Wraps a database transaction in a 'with' scope.
        Once the transaction finishes successfully, any object given as an
        argument will be refreshed. """
    try:
        yield
        session.commit()
        for arg in args:
            session.refresh(arg)
    except:
        session.rollback()
        raise
