from sqlalchemy import create_engine, asc, desc
from sqlalchemy.orm import sessionmaker, scoped_session, joinedload, subqueryload

from clean_demo.database import Database, DatabaseSession, DatabaseOrder, DatabaseLoader

from .schema import user_table, post_table


class SqlAlchemyDatabase(Database):
    class SqlAlchemyOrder(DatabaseOrder):
        def ascending(self, attribute):
            return asc(attribute)

        def descending(self, attribute):
            return desc(attribute)

    class SqlAlchemyLoader(DatabaseLoader):
        def load_one_to_one(self, attribute):
            return joinedload(attribute)

        def load_one_to_many(self, attribute):
            return subqueryload(attribute)

    def __init__(self, uri: str, echo=False) -> None:
        self.order = self.SqlAlchemyOrder()
        self.loader = self.SqlAlchemyLoader()
        self.engine = create_engine(uri, echo=echo)
        self.Session = scoped_session(sessionmaker(self.engine))

    def get_session(self) -> DatabaseSession:
        return self.Session()

    def remove_session(self) -> None:
        self.Session.remove()
