from sqlalchemy import Table, Column, Integer, String, ForeignKey
from sqlalchemy.orm import mapper, relationship

from clean_demo.model import Post

from .metadata import metadata
from .user import user_mapper

__all__ = ["post_table", "post_mapper"]

post_table = Table("post", metadata,
                   Column("post_id", Integer, primary_key=True),
                   Column("content", String),
                   Column("author_id", Integer, ForeignKey("user.user_id")))

post_mapper = mapper(Post, post_table, properties={
    "author": relationship(user_mapper, uselist=False),
})
