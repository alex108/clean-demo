from sqlalchemy import Table, Column, Integer, String
from sqlalchemy.orm import mapper

from clean_demo.model import User

from .metadata import metadata

__all__ = ["user_table", "user_mapper"]

user_table = Table("user", metadata,
                   Column("user_id", Integer, primary_key=True),
                   Column("username", String),
                   Column("password", String),
                   Column("email", String))

user_mapper = mapper(User, user_table, properties={
    "_password": user_table.c.password,
})
