import clean_demo.model.user

__all__ = ["Post"]


class Post(object):
    post_id: int
    content: str
    author: "clean_demo.model.user.User"

    def __init__(self, post_id: int=None, content: str=None,
                 author: "clean_demo.model.user.User"=None) -> None:
        self.post_id = post_id
        self.content = content
        self.author = author
