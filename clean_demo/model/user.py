from typing import Collection

import bcrypt

import clean_demo.model.post

__all__ = ["User"]


class User(object):
    user_id: int
    username: str
    email: str
    posts: Collection["clean_demo.model.post.Post"]

    _password: bytes

    def __init__(self, user_id: int=None, username: str=None, password: str=None, email: str=None) -> None:
        self.user_id = user_id
        self.username = username
        self._password = None  # Hasing done in setter
        self.password = password
        self.email = email

    def check_password(self, password: str) -> bool:
        return bcrypt.checkpw(password.encode(), self._password)

    @property
    def password(self) -> str:
        return self._password.decode()

    @password.setter
    def password(self, password) -> None:
        salt = bcrypt.gensalt()
        self._password = bcrypt.hashpw(password.encode(), salt)
