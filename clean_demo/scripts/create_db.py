from sqlalchemy import create_engine

from clean_demo.database.sqlalchemy.schema.metadata import metadata

engine = create_engine("sqlalchemy:///test.db", echo=True)

metadata.create_all(bind=engine)
