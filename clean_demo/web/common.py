from functools import wraps
from typing import List

from cerberus import Validator
from flask import g, request, abort
from flask.blueprints import Blueprint

__all__ = ["Router", "route", "validate"]


class Router(object):
    blueprint: Blueprint

    def __init__(self, blueprint: Blueprint) -> None:
        self.blueprint = blueprint
        super(Router, self).__init__()
        routes = [getattr(self, f) for f in dir(self) if callable(getattr(self, f))
                  and not f.startswith("__")
                  and "is_route" in getattr(self, f).__dict__]
        for rt in routes:
            self.blueprint.add_url_rule(rt.url, rt.name, rt, methods=rt.methods)


def route(url: str, methods: List[str]):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            return f(*args, **kwargs)

        wrapper.name = f.__name__
        wrapper.is_route = True
        wrapper.url = url
        wrapper.methods = methods
        return wrapper

    return decorator


def validate(validator: Validator):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            body = request.form
            valid = validator.validate(body)
            if not valid:
                return abort(400)
            g.body = body
            return f(*args, **kwargs)

        return wrapper
    return decorator
